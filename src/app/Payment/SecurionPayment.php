<?php
namespace App\Payment;

use SecurionPay\SecurionPayGateway;
use SecurionPay\Request\CustomerRequest;
use SecurionPay\Request\ChargeRequest;
use SecurionPay\Request\SubscriptionRequest;
use SecurionPay\Exception\SecurionPayException;

class SecurionPayment{

    public static function processPaymentInfo($paymentInfo){

        $securionPay = new SecurionPayGateway(env('SECURIONPAY_SECRET'));
        $subscriptionRequest = new SubscriptionRequest();

        $customerRequest = new CustomerRequest();
        $customerRequest->email($paymentInfo['email'])->card($paymentInfo['tokenId']);
        $customer = $securionPay->createCustomer($customerRequest);
        
        $chargeRequest = new ChargeRequest();
        $chargeRequest->amount(2000)->currency('USD')->customerId($customer->getId());
             
        try{
            //$charge = $securionPay->createCharge($chargeRequest);
            //$chargeId = $charge->getId();
            $subscriptionRequest->customerId($customer->getId())->planId(env('SECURIONPAY_PLAN'));
            $subscription = $securionPay->createSubscription($subscriptionRequest);
            $subscriptionId = $subscription->getId();

            // if($chargeId <> null)
            // {
            //     $subscriptionRequest->customerId($customer->getId())->planId(env('SECURIONPAY_PLAN'));
            //     $subscription = $securionPay->createSubscription($subscriptionRequest);
            // }

            return [
                'success' => true, 
                'payload' => $subscriptionId
            ];

        } catch (SecurionPayException $e) {
            return [
                'success' => false, 
                'type' => $e->getType(),
                'code' => $e->getCode(),
                'message' => $e->getMessage()
            ];
        }
        
        return ['success' => false, 'message' => 'something went wrong'];
    }
}