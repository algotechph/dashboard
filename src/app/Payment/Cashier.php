<?php
namespace App\Payment;

use Exception;
use Twocheckout;
use Twocheckout_Charge;

class Cashier{

    public static function processPaymentInfo($paymentInfo){
        
        Twocheckout::privateKey(env('2CHECKOUT_PRIVATE_KEY'));
        Twocheckout::sellerId(env('2CHECKOUT_SELLER_ID'));

        Twocheckout::username(env('2CHECKOUT_USERNAME'));
        Twocheckout::password(env('2CHECKOUT_ACCOUNT_KEY'));

        // Twocheckout::verifySSL(!(bool)(env('2CHECKOUT_SANDBOX')));

        // If you want to turn off SSL verification (Please don't do this in your production environment)
        Twocheckout::verifySSL(env('2CHECKOUT_SANDBOX'));  // this is set to true by default

        // All methods return an Array by default or you can set the format to 'json' to get a JSON response.
        Twocheckout::format('json');
        //Twocheckout::sandbox(true);

        try{
            $charge = Twocheckout_Charge::auth($paymentInfo, 'array');
            if($charge['response']['responseCode'] == 'APPROVED')
            {
                return ['success' => true, 'payload' => $charge];
            }
        } catch (Exception $e) {
            return ['success' => false, 'message' => $e->getMessage()];
        }

        return ['success' => false, 'message' => 'something went wrong'];
    }
}