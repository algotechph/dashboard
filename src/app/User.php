<?php
namespace App;

use Illuminate\Contracts\Auth\MustVerifyEmail;
//use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use GoldSpecDigital\LaravelEloquentUUID\Foundation\Auth\User as Authenticatable;
use Illuminate\Support\Facades\Hash;

class User extends Authenticatable
{
    use Notifiable;
    
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $guarded = [];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function getFullNameAttribute()
    {
        // if (is_null($this->last_name)) {
        //     return "{$this->name}";
        // }

        // return "{$this->name} {$this->last_name}";
        return "{$this->first_name} {$this->last_name}";
    }

    public function setPasswordAttribute($pass)
    {
        $this->attributes['password'] = Hash::make($pass);
    }

    public function campaigns()
    {
        return $this->hasMany('Modules\Campaign\Entities\Campaign', 'user_id');
    }

    public function payments()
    {
        return $this->hasMany('Modules\Payment\Entities\Payment', 'user_id');
    }

    public function agent_manager()
    {
        return $this->belongsTo('App\User', 'manager');
    }
}
