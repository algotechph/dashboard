<?php
namespace App\Http\Controllers;

use App\User;
use Auth;
use Modules\Agent\Entities\AgentCampaign;
use Modules\Campaign\Entities\Campaign;
use Modules\Payment\Entities\Payment;
use Illuminate\Http\Request;
use App\Http\Traits\LeadAnalyticsTrait;

class HomeController extends Controller
{
    use LeadAnalyticsTrait;
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $users = User::count();

        $widget = [
            'users' => $users,
            //...
        ];

        $analytics = $this->getLeadSummary();

        if(Auth::user()->type == 1)
        {
            $campaigns = AgentCampaign::where('user_id', Auth::user()->id)->get();
        }
        elseif(Auth::user()->type == 2)
        {
            $campaigns = AgentCampaign::all();
        }
        else
        {
            $campaigns = Campaign::all();
        }
       
        return view('dashboard', compact('widget', 'analytics', 'campaigns'));
    }
}
