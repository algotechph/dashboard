<?php
namespace App\Http\Traits;

use Illuminate\Support\Facades\DB;
use Carbon\Carbon; 

trait EncryptionTrait {
    protected $key;

    public function __construct()
    {
        $this->key = 'f5746e23-a569-4ab3-a1bb-c5981d33b49b';
    }

    public function encrypt($data, $key)
    {
         // Remove the base64 encoding from our key
        $encryption_key = base64_decode($key);
        // Generate an initialization vector
        $iv = openssl_random_pseudo_bytes(openssl_cipher_iv_length('aes-256-cbc'));
        // Encrypt the data using AES 256 encryption in CBC mode using our encryption key and initialization vector.
        $encrypted = openssl_encrypt($data, 'aes-256-cbc', $encryption_key, 0, $iv);
        // The $iv is just as important as the key for decrypting, so save it with our encrypted data using a unique separator (::)
        return base64_encode($encrypted . '::' . $iv);
    }

    public function decrypt($data, $key)
    {
        // Remove the base64 encoding from our key
        $encryption_key = base64_decode($key);
        // To decrypt, split the encrypted data from our IV - our unique separator used was "::"
        list($encrypted_data, $iv) = explode('::', base64_decode($data), 2);
        return openssl_decrypt($encrypted_data, 'aes-256-cbc', $encryption_key, 0, $iv);
    }

    public function check($token)
    {
        #Decrypt token
        $key = $this->$key;
        $dec = decrypt($token, $key);
        
        #Check the validity
        $datetime1 = new DateTime($dec);
        $datetime2 = new DateTime(date('Y-m-d H:i:s'));
        $interval = $datetime1->diff($datetime2);
        $elapsed = $interval->format('%s');
        
        return ($elapsed > VALIDITY) ? true : false;
    }

}