<?php
namespace App\Http\Traits;

use Illuminate\Support\Facades\DB;
use Modules\Agent\Entities\AgentCampaign;
use Modules\Payment\Entities\Payment;
use Carbon\Carbon; 
use Auth;

trait LeadAnalyticsTrait {
    
    public function getLeadSummary()
    {
        $data = array(
            'earnings_month' => ($this->getEarnings('month') <> "[]" ? $this->getEarnings('month') : null),
            'earnings_today' => ($this->getEarnings('today') <> "[]" ? $this->getEarnings('today') : null),
            'earnings_daily' => ($this->getEarnings('daily') <> "[]" ? $this->getEarnings('daily') : null),
            'campaign_views' => $this->getAllCampaignStats('views'),
            'earnings_last_month' => ($this->getEarningsLastMonth('month') <> "[]" ? $this->getEarningsLastMonth('month') : null),
            'earnings_last_month_daily' => ($this->getEarningsLastMonth('daily') <> "[]" ? $this->getEarningsLastMonth('daily') : null),
            'agent_campaign_views_today' => $this->getAgentAnalytics('today'),
            'agent_campaign_views_month' => $this->getAgentAnalytics('month'),
            'agent_subscriptions_today' => ($this->getAgentSubcriptions('today') <> "[]" ? $this->getAgentSubcriptions('today') : null),
            'agent_month_subscriptions' => ($this->getAgentSubcriptions('month') <> "[]" ? $this->getAgentSubcriptions('month') : null),
        );
        //dump($this->getAgentSubcriptions('today'));
        //  echo $data['earnings_last_month'];
        //  echo '<pre>';
        //  print_r($data);
        //  echo '</pre>';
         //die();

        return $data;
    }

    public function getAllCampaignStats($stat)
    { 
        if($stat == 'views') 
        {
            $stats = AgentCampaign::whereMonth('created_at', '=', Carbon::now()->format('m'))->get()->sum($stat);
        }
        else
        {
            $stats = AgentCampaign::whereDay('created_at', '=', Carbon::now()->format('d'))->get()->sum($stat);
        }            

        return $stats;
    }

    public function getAgentAnalytics($stat)
    {
        if($stat == 'month')
        {
            $stats = AgentCampaign::where('user_id', Auth::user()->id)
                ->whereMonth('created_at', '=', Carbon::now()->format('m'))
                ->get()
                ->sum('views');
        }
        else
        {
            $stats = AgentCampaign::where('user_id', Auth::user()->id)
                ->whereMonth('created_at', '=', Carbon::now()->format('m'))
                ->whereDay('created_at', '=', Carbon::now()->format('d'))
                ->get()
                ->sum('views');
        }
        
        return $stats;
    }

    public function getAgentSubcriptions($type = null)
    {
        $count = Payment::count();
        $subscriptions = array();
        if($count <> 0)
        {
            if($type == 'month')
            {
                $subscriptions = Payment::select(DB::raw('MONTH(created_at) as month, YEAR(created_at), DAY(created_at) as day, COUNT(*) as total_subscriptions'))
                        ->where('user_id', Auth::user()->id)
                        ->whereMonth('created_at', '=', Carbon::now()->format('m'))
                        ->whereYear('created_at', '=', Carbon::now()->format('Y'))
                        ->groupBy(DB::raw('YEAR(created_at), MONTH(created_at), DAY(created_at)'))
                        ->orderBy(DB::raw('YEAR(created_at), MONTH(created_at), DAY(created_at)'))
                        ->get();
            }
            else
            {
                $subscriptions = Payment::select(DB::raw('MONTH(created_at) as month, YEAR(created_at), DAY(created_at) as day, COUNT(*) as total_subscriptions'))
                        ->where('user_id', Auth::user()->id)
                        ->whereDay('created_at', '=', Carbon::now()->format('d'))
                        ->whereMonth('created_at', '=', Carbon::now()->format('m'))
                        ->whereYear('created_at', '=', Carbon::now()->format('Y'))
                        ->groupBy(DB::raw('YEAR(created_at), MONTH(created_at), DAY(created_at)'))
                        ->orderBy(DB::raw('YEAR(created_at), MONTH(created_at), DAY(created_at)'))
                        ->get();
            }
        }
        return json_encode($subscriptions);
    }

    public function getEarnings($type = null)
    {
        $count = Payment::count();
        $earnings = array();
        if($count <> 0)
        {
            if($type == 'month')
            {
                $earnings = Payment::select(DB::raw('MONTH(created_at) as month, YEAR(created_at), COUNT(*) as total_leads, SUM(amount) AS total_amount'))
                    ->whereMonth('created_at', '=', Carbon::now()->format('m'))
                    ->whereYear('created_at', '=', Carbon::now()->format('Y'))
                    ->groupBy(DB::raw('YEAR(created_at), MONTH(created_at)'))
                    ->orderBy(DB::raw('YEAR(created_at), MONTH(created_at)'))
                    ->get();
            }
            else if($type == 'daily')
            {
                $earnings = Payment::select(DB::raw('MONTH(created_at) as month, YEAR(created_at),  DAY(created_at) as day, COUNT(*) as total_leads, SUM(amount) AS total_amount'))
                    ->whereMonth('created_at', '=', Carbon::now()->format('m'))
                    ->whereYear('created_at', '=', Carbon::now()->format('Y'))
                    ->groupBy(DB::raw('YEAR(created_at), MONTH(created_at), DAY(created_at)'))
                    ->orderBy(DB::raw('YEAR(created_at), MONTH(created_at), DAY(created_at)'))
                    ->get();
            }
            else
            {
                $earnings = Payment::select(DB::raw('MONTH(created_at) as month, YEAR(created_at), DAY(created_at) as day, COUNT(*) as total_leads, SUM(amount) AS total_amount'))
                    ->whereMonth('created_at', '=', Carbon::now()->format('m'))
                    ->whereDay('created_at', '=', Carbon::now()->format('d'))
                    ->whereYear('created_at', '=', Carbon::now()->format('Y'))
                    ->groupBy(DB::raw('YEAR(created_at), MONTH(created_at), DAY(created_at)'))
                    ->orderBy(DB::raw('YEAR(created_at), MONTH(created_at), DAY(created_at)'))
                    ->get();
            }
            
        }
        return json_encode($earnings);

    }

    public function getEarningsLastMonth($type = null)
    {
        $count = Payment::count();
        $earnings = array();
        if($count <> 0)
        {
            if($type == 'month')
            {
                $earnings = Payment::select(DB::raw('MONTH(created_at) as month, YEAR(created_at), COUNT(*) as total_leads, SUM(amount) AS total_amount'))
                    ->whereMonth('created_at', '=', Carbon::now()->subMonth()->format('m'))
                    ->whereYear('created_at', '=', Carbon::now()->format('Y'))
                    ->groupBy(DB::raw('YEAR(created_at), MONTH(created_at)'))
                    ->orderBy(DB::raw('YEAR(created_at), MONTH(created_at)'))
                    ->get();
            }
            else if($type == 'daily')
            {
                $earnings = Payment::select(DB::raw('MONTH(created_at) as month, YEAR(created_at),  DAY(created_at) as day, COUNT(*) as total_leads, SUM(amount) AS total_amount'))
                    ->whereMonth('created_at', '=', Carbon::now()->subMonth()->format('m'))
                    ->whereYear('created_at', '=', Carbon::now()->format('Y'))
                    ->groupBy(DB::raw('YEAR(created_at), MONTH(created_at), DAY(created_at)'))
                    ->orderBy(DB::raw('YEAR(created_at), MONTH(created_at), DAY(created_at)'))
                    ->get();
            }
            else
            {
                $earnings = Payment::select(DB::raw('MONTH(created_at) as month, YEAR(created_at), DAY(created_at) as day, COUNT(*) as total_leads, SUM(amount) AS total_amount'))
                    ->whereMonth('created_at', '=', Carbon::now()->subMonth()->format('m'))
                    ->whereDay('created_at', '=', Carbon::now()->format('d'))
                    ->whereYear('created_at', '=', Carbon::now()->format('Y'))
                    ->groupBy(DB::raw('YEAR(created_at), MONTH(created_at), DAY(created_at)'))
                    ->orderBy(DB::raw('YEAR(created_at), MONTH(created_at), DAY(created_at)'))
                    ->get();
            }
            
        }
        return json_encode($earnings);

    }
}