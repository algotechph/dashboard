<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return redirect()->route('dashboard');
});

Route::get('/home', function () {
    return redirect()->route('dashboard');
});

Auth::routes();

Route::group(['middleware' => ['web', 'auth']], function (){
    Route::get('/dashboard', 'HomeController@index')->name('dashboard');
    
    //Route::get('/home', 'HomeController@index')->name('home');

    Route::get('/profile', 'ProfileController@index')->name('profile');
    Route::put('/profile', 'ProfileController@update')->name('profile.update');

    // Route::get('/about', function () {
    //     return view('about');
    // })->name('about');
});

Route::get('/clear-cache', function() {
    Artisan::call('optimize:clear');
    dump(Artisan::output());
});
