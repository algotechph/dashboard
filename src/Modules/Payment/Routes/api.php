<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

// Route::middleware('auth:api')->get('/payment', function (Request $request) {
//     return $request->user();
// });
Route::group(['middleware' => 'api'], function () {
    //Route::post('/process/payment', 'PaymentController@processPayment')->name('client.process-payment');
    Route::post('/process/paymentIntent', 'PaymentController@setPaymentIntent')->name('client.process-payment-intent');
    Route::post('/process/payment', 'PaymentController@processPayment');
    Route::post('/sales', 'PaymentController@getSales')->name('ajax.get-sales');

    Route::get('/process/affiliate/{id}/{email}/{amount}', 'PaymentController@postback');
});
