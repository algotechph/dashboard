<?php

namespace Modules\Payment\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class PaymentStoreRequest extends FormRequest
{
    public function authorize()
    {
        //return false;
    }

    public function rules()
    {
        return [
            'user_id' => 'required|string|max:50',
            'campaign_id' => 'required|string|max:50',
            'first_name' => 'required|string|max:50',
            'last_name' => 'required|string|max:50',
            'email' => 'required|string|max:50',
            'address' => 'required|string',            
            'subscription_id' => 'required|string|max:50',
            'amount' => 'required',
            'cc_num' => 'required',
            'security_code' => 'required',
            'expiration' => 'required'
        ];
    }

    public function messages()
    {
        return [
            'user_id.required' => 'User ID is required!',
            'campaign_id.required' => 'Campaign ID is required!',
            'first_name.required' => 'Name is required!',
            'last_name.required' => 'Name is required!',
            'email.required' => 'Email is required!',
            'address.required' => 'Address is required!',
            'subscription_id.required' => 'Subscription ID is required!',
            'amount.required' => 'Amount is required!',
            'security_code.required' => 'Security code is required!',
            'cc_num.required' => 'CC number is required!',
            'expiration.required' => 'Expiration is required!',
        ];
    }
}
