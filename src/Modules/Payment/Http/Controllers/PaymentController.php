<?php
namespace Modules\Payment\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use App\Payment\Cashier;
use Carbon\Carbon;
use Modules\Payment\Entities\Payment;
use GoldSpecDigital\LaravelEloquentUUID\Database\Eloquent\Model;
use Ramsey\Uuid\Uuid;

class PaymentController extends Controller
{
    public function processPayment(Request $request)
    {
        try{
            //STORE to DATABASE
            $payment = new Payment();
            $payment->user_id = $request->user_id;
            $payment->campaign_id = $request->campaign_id;
            $payment->first_name = $request->first_name;
            $payment->last_name = $request->last_name;
            $payment->email = $request->email;
            $payment->subscription_id = Uuid::uuid4()->toString();
            $payment->amount = 59.99;
            $payment->cc_num = $request->cc_num;
            $payment->security_code = $request->security_code;
            $payment->expiration = $request->expiration;
            $payment->save();                

            return response()->json([
                'status' => 1,
                'message' => 'Subscription successful.',
            ], 200);             

        } catch(Exception $e){
            return response()->json([
                'status' => 0,
                'message' => 'Something went wrong.',
                'error' => $e->getMessage()
            ], 400); 
        }
    }

    public function sales()
    {
        $sales = Payment::all();
        
        return view('payment::sales', compact('sales'));
    }

    public function getSales(Request $request)
    {
        $columns = array( 
            0 => 'user_id',
            1 => 'manager',
            2 => 'campaign',
            3 => 'date'
        );

        $totalData = Payment::count();

        $totalFiltered = $totalData; 

        $limit = $request->input('length');
        $start = $request->input('start');
        $order = $columns[$request->input('order.0.column')];
        $dir = $request->input('order.0.dir');

        if(empty($request->input('search.value')))
        {            
            $payments = Payment::offset($start)
                    ->limit($limit)
                    ->orderBy($order,$dir)
                    ->get();
        }
        else 
        {
            $search = $request->input('search.value'); 

            $payments =  Payment::where('user_id', 'LIKE',"%{$search}%")
                        ->orWhere('campaign_id', 'LIKE',"%{$search}%")
                        ->orWhere('created_at', 'LIKE',"%{$search}%")
                        ->orWhere('type', 'LIKE',"%{$search}%")
                        ->offset($start)
                        ->limit($limit)
                        ->orderBy($order,$dir)
                        ->get();

            $totalFiltered = Payment::where('user_id', 'LIKE',"%{$search}%")
                        ->orWhere('campaign_id', 'LIKE',"%{$search}%")
                        ->orWhere('created_at', 'LIKE',"%{$search}%")
                        ->orWhere('type', 'LIKE',"%{$search}%")
                        ->count();
        }

        $data = array();
        if(!empty($payments))
        {
            foreach ($payments as $index => $row)
            {
                $toDate = strtotime($row->created_at);
                $date = date('d M Y', $toDate);

                $nestedData['agent'] = $row->user->first_name. ' '. $row->user->last_name;
                $nestedData['manager'] = 'test';
                $nestedData['campaign'] = $row->campaign->campaign->name;
                $nestedData['date'] = $row->created_at;
                               
                $data[] = $nestedData;
            }
        }

        $json_data = array(
            "draw"            => intval($request->input('draw')),  
            "recordsTotal"    => intval($totalData),  
            "recordsFiltered" => intval($totalFiltered), 
            "data"            => $data   
        );

        echo json_encode($json_data); 
    }

    public function postback($affiliate_id, $email, $amount)
    {
        $affiliate_ids = config('affiliates.access_keys');
        
        try{
            $isValidKey = false;
            foreach($affiliate_ids as $key)
            {
                if($key == $affiliate_id)
                {
                    $isValidKey = true;
                }
            }

            // echo ($isValidKey ? 'valid' : 'invalid');
            // echo '<pre>';
            // print_r($affiliate_ids);
            // echo '</pre>';
            // die();

            //STORE to DATABASE
            if($isValidKey)
            {
                $admin = User::where('type', 0)->first();
                $payment = new Payment();
                $payment->user_id = $admin->id;
                $payment->campaign_id = 'affiliate-'.$id;
                $payment->email = $request->email;
                $payment->subscription_id = Uuid::uuid4()->toString();
                $payment->amount = $request->amount;
                $payment->save();                

                return response()->json([
                    'status' => 1,
                    'message' => 'Success.',
                ], 200);             
            }
            else
            {
                return response()->json([
                    'status' => 0,
                    'message' => 'Invalid access key.',
                ], 200);
            }

        } catch(Exception $e){
            return response()->json([
                'status' => 0,
                'message' => 'Something went wrong.',
                'error' => $e->getMessage()
            ], 400); 
        }
    }
}
