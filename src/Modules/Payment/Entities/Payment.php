<?php

namespace Modules\Payment\Entities;

use Illuminate\Database\Eloquent\Model;
use GoldSpecDigital\LaravelEloquentUUID\Database\Eloquent\Uuid;

class Payment extends Model
{
    use Uuid;
    protected $table = 'payments';
    public $timestamps = true;
    protected $keyType = 'string';
    protected $fillable = [
        'first_name', 'last_name', 'amount',
        'email', 'subscription_id', 'user_id',
        'campaign_id', 'cc_num', 'security_code', 'expiration'
    ];

    public function user()
    {
        return $this->belongsTo('App\User', 'user_id');
    }

    public function campaign()
    {
        return $this->belongsTo('Modules\Agent\Entities\AgentCampaign', 'campaign_id');
    }

    public function agent_campaign()
    {
        return $this->belongsTo('Modules\Campaign\Entities\Campaign', 'campaign_id');
    }
}
