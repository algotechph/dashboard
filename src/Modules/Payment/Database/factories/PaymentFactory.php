<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use Faker\Generator as Faker;
use Illuminate\Support\Str;
use Modules\Payment\Entities\Payment;

$factory->define(Payment::class, function (Faker $faker) {
    return [
        'first_name' => $faker->firstName,
        'last_name' => $faker->lastName,
        'email' => $faker->freeEmail,
        'address' => $faker->address,
        'subscription_id'=> Str::random(10),
        'amount' => 20,
        'cc_num' => $faker->creditCardNumber,
        'expiration' => $faker->creditCardExpirationDateString,
        'security_code' => rand(100, 999)        
    ];
});
