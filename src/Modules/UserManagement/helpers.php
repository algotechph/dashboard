<?php

namespace Modules\UserManagement;

class Helper{

    public function password_generate() 
    {
      $data = '1234567890ABCDEFGHIJKLMNOPQRSTUVWXYZabcefghijklmnopqrstuvwxyz';
      
      return substr(str_shuffle($data), 0, rand(6, 10));
    }
}