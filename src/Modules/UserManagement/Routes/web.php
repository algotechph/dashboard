<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::prefix('user-management')->middleware('auth')->group(function() {
    Route::get('/', 'UserManagementController@index')->name('user-management.index');
    Route::get('/user/create', 'UserManagementController@create')->name('user-management.create-user');
    Route::post('/user/create', 'UserManagementController@store')->name('user-management.store-user');
    Route::get('/user/edit/{user_id}', 'UserManagementController@edit')->name('user-management.edit-user');
    Route::post('/user/update/{user_id}', 'UserManagementController@update')->name('user-management.update-user');
    Route::get('/user/delete/{user_id}', 'UserManagementController@destroy')->name('user-management.destroy-user');
});
