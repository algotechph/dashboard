<?php

namespace Modules\UserManagement\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Modules\UserManagement\Helper;
use Illuminate\Support\Facades\Hash;
use Auth;
use App\User;
use Modules\UserManagement\Http\Requests\UserStoreRequest;
use Modules\UserManagement\Http\Requests\UserUpdateRequest;
use Modules\Agent\Entities\AgentCampaign;

class UserManagementController extends Controller
{
    private $helper;

    public function __construct()
    {
        $this->helper = new Helper();
    }

    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index()
    {
        return view('usermanagement::index');
    }

    /**
     * [getRecords Ajax function for retrieving users and optimizing load time of datatable]
     *
     * @param   Request  $request  [$request description]
     *
     * @return  [type]             [return description]
     */
    public function getUsers(Request $request)
    {
        $columns = array( 
            0 => 'first_name',
            1 => 'last_name',
            2 => 'email',
            3 => 'type',
            4 => 'actions'
        );
        $totalData = User::count();
       
        $totalFiltered = $totalData; 

        $limit = $request->input('length');
        $start = $request->input('start');
        $order = $columns[$request->input('order.0.column')];
        $dir = $request->input('order.0.dir');

        if(empty($request->input('search.value')))
        {            
            $users = User::offset($start)
                    ->limit($limit)
                    ->orderBy($order,$dir)
                    ->get();
        }
        else 
        {
            $search = $request->input('search.value'); 

            $users =  User::where('first_name', 'LIKE',"%{$search}%")
                        ->orWhere('last_name', 'LIKE',"%{$search}%")
                        ->orWhere('email', 'LIKE',"%{$search}%")
                        ->orWhere('type', 'LIKE',"%{$search}%")
                        ->offset($start)
                        ->limit($limit)
                        ->orderBy($order,$dir)
                        ->get();

            $totalFiltered = User::where('first_name', 'LIKE',"%{$search}%")
                        ->orWhere('last_name', 'LIKE',"%{$search}%")
                        ->orWhere('email', 'LIKE',"%{$search}%")
                        ->orWhere('type', 'LIKE',"%{$search}%")
                        ->count();
        }

        $data = array();
        if(!empty($users))
        {
            foreach ($users as $index => $row)
            {
                $type = '';
                if($row->type == 0)
                {
                    $type = 'Admin';
                }
                elseif($row->type == 1)
                {
                    $type = 'Agent';
                }
                else
                {
                    $type = 'Manager';
                }
                $toDate = strtotime($row->created_at);
                $date = date('d M Y', $toDate);

                $nestedData['first_name'] = $row->first_name;
                $nestedData['last_name'] = $row->last_name;
                $nestedData['email'] = $row->email;
                $nestedData['type'] = $type;
                $nestedData['actions'] = '<a class="btn btn-success btn-sm" href="'.route('user-management.edit-user', $row->id).'" role="button">Edit</a> 
                <a class="btn btn-danger btn-sm" href="'.route('user-management.destroy-user', $row->id).'" role="button">Delete</a>';
                
                $data[] = $nestedData;
            }
        }

        $json_data = array(
            "draw"            => intval($request->input('draw')),  
            "recordsTotal"    => intval($totalData),  
            "recordsFiltered" => intval($totalFiltered), 
            "data"            => $data   
        );

        echo json_encode($json_data); 
    }

    public function getManagersAgent(Request $request)
    {
        $columns = array( 
            0 => 'first_name',
            1 => 'last_name',
            2 => 'email',
            3 => 'type',
            4 => 'actions'
        );
        $totalData = User::where('manager', $request->id)->get();
        $totalData = count($totalData);

        $totalFiltered = $totalData; 

        $limit = $request->input('length');
        $start = $request->input('start');
        $order = $columns[$request->input('order.0.column')];
        $dir = $request->input('order.0.dir');

        if(empty($request->input('search.value')))
        {            
            $users = User::offset($start)
                    ->where('manager', $request->id)
                    ->limit($limit)
                    ->orderBy($order,$dir)
                    ->get();
        }
        else 
        {
            $search = $request->input('search.value'); 

            $users =  User::where('first_name', 'LIKE',"%{$search}%")
                        ->orWhere('last_name', 'LIKE',"%{$search}%")
                        ->orWhere('email', 'LIKE',"%{$search}%")
                        ->orWhere('type', 'LIKE',"%{$search}%")
                        ->offset($start)
                        ->limit($limit)
                        ->orderBy($order,$dir)
                        ->get();

            $totalFiltered = User::where('first_name', 'LIKE',"%{$search}%")
                        ->orWhere('last_name', 'LIKE',"%{$search}%")
                        ->orWhere('email', 'LIKE',"%{$search}%")
                        ->orWhere('type', 'LIKE',"%{$search}%")
                        ->count();
        }

        $data = array();
        if(!empty($users))
        {
            foreach ($users as $index => $row)
            {
                $type = '';
                if($row->type == 0)
                {
                    $type = 'Admin';
                }
                elseif($row->type == 1)
                {
                    $type = 'Agent';
                }
                else
                {
                    $type = 'Manager';
                }
                $toDate = strtotime($row->created_at);
                $date = date('d M Y', $toDate);

                $nestedData['first_name'] = $row->first_name;
                $nestedData['last_name'] = $row->last_name;
                $nestedData['email'] = $row->email;
                $nestedData['type'] = $type;
                $nestedData['actions'] = '<a class="btn btn-success btn-sm" href="'.route('user-management.edit-user', $row->id).'" role="button">Edit</a> 
                <a class="btn btn-danger btn-sm" href="'.route('user-management.destroy-user', $row->id).'" role="button">Delete</a>';
                
                $data[] = $nestedData;
            }
        }

        $json_data = array(
            "draw"            => intval($request->input('draw')),  
            "recordsTotal"    => intval($totalData),  
            "recordsFiltered" => intval($totalFiltered), 
            "data"            => $data   
        );
        echo json_encode($json_data);
    }

    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create()
    {
        $password = $this->helper->password_generate();

        return view('usermanagement::create', compact('password'));
    }

    /**
     * Store a newly created resource in storage.
     * @param Request $request
     * @return Response
     */
    public function store(UserStoreRequest $request)
    {
        User::create($request->all());
        return back()->with('status', 'User created.');
    }
    /**
     * Show the specified resource.
     * @param int $id
     * @return Response
     */
    public function show($id)
    {
        return view('usermanagement::show');
    }

    /**
     * Show the form for editing the specified resource.
     * @param int $id
     * @return Response
     */
    public function edit($id)
    {
        $user = User::find($id);
        
        return view('usermanagement::edit', compact('user'));
    }

    /**
     * Update the specified resource in storage.
     * @param Request $request
     * @param int $id
     * @return Response
     */
    public function update(UserUpdateRequest $request, $id)
    {
        $user = User::find($id);
        $user->first_name = $request->first_name;
        $user->last_name = $request->last_name;
        $user->email = $request->email;
        $user->password = $request->password;
        $user->type = $request->type;
        
        $user->save();

        return back()->with('status', 'User updated.');
    }

    /**
     * Remove the specified resource from storage.
     * @param int $id
     * @return Response
     */
    public function destroy($id)
    {
        $user = User::find($id);
        $user->delete();
        $agent_campaigns = AgentCampaign::where('user_id', $id)->pluck('id');
        if(count($agent_campaigns) <> null)
        {
            AgentCampaign::destroy($agent_campaigns->toArray());             
        } 

        return back();    
    }
}
