<?php

namespace Modules\UserManagement\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UserUpdateRequest extends FormRequest
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'email' => ['required', 'email', \Illuminate\Validation\Rule::unique('users')->ignore($this->id)],
            'first_name' => 'required|string|max:50',
            'last_name' => 'required|string|max:50',
            'password' => 'required|min:6'
        ];
    }

    public function messages()
    {
        return [
            'email.required' => 'Email is required!',
            'first_name.required' => 'Name is required!',
            'last_name.required' => 'Name is required!',
            'password.required' => 'Password is required!'
        ];
    }
}
