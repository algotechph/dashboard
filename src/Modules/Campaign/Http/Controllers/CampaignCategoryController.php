<?php

namespace Modules\Campaign\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Modules\Campaign\Entities\CampaignCategory;
use Modules\Campaign\Http\Requests\CampaignCategoryStoreRequest;
use Modules\Campaign\Http\Requests\CampaignCategoryUpdateRequest;

class CampaignCategoryController extends Controller
{
    /**
     * [getRecords Ajax function for retrieving users and optimizing load time of datatable]
     *
     * @param   Request  $request  [$request description]
     *
     * @return  [type]             [return description]
     */
    public function getCampaignCategory(Request $request)
    {
        $columns = array( 
            0 => 'name',
            1 => 'actions'
        );

        $totalData = CampaignCategory::count();

        $totalFiltered = $totalData; 

        $limit = $request->input('length');
        $start = $request->input('start');
        $order = $columns[$request->input('order.0.column')];
        $dir = $request->input('order.0.dir');

        if(empty($request->input('search.value')))
        {            
            $campaigns = CampaignCategory::offset($start)
                    ->limit($limit)
                    ->orderBy($order,$dir)
                    ->get();
        }
        else 
        {
            $search = $request->input('search.value'); 

            $campaigns =  CampaignCategory::where('name', 'LIKE',"%{$search}%")
                        ->offset($start)
                        ->limit($limit)
                        ->orderBy($order,$dir)
                        ->get();

            $totalFiltered = CampaignCategory::where('name', 'LIKE',"%{$search}%")
                        ->count();
        }

        $data = array();
        if(!empty($campaigns))
        {
            $category_name = null;
            foreach ($campaigns as $index => $row)
            {
                $category_name = CampaignCategory::find($row->category_id);
                $toDate = strtotime($row->created_at);
                $date = date('d M Y', $toDate);

                $nestedData['name'] = $row->name;
                $nestedData['actions'] = '<a class="btn btn-success btn-sm" href="'.route('campaign-management.edit-campaign-category', $row->id).'" role="button">Edit</a> 
                                        <a class="btn btn-danger btn-sm" href="'.route('campaign-management.destroy-campaign-category', $row->id).'" role="button">Delete</a>';
                                
                $data[] = $nestedData;
            }
        }

        $json_data = array(
            "draw"            => intval($request->input('draw')),  
            "recordsTotal"    => intval($totalData),  
            "recordsFiltered" => intval($totalFiltered), 
            "data"            => $data   
        );

        echo json_encode($json_data); 
    }

    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index()
    {
        return view('modules.campaign.categories.index');
    }

    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create()
    {
        return view('modules.campaign.categories.create');
    }

    /**
     * Store a newly created resource in storage.
     * @param Request $request
     * @return Response
     */
    public function store(CampaignCategoryStoreRequest $request)
    {
        CampaignCategory::create($request->all());

        return back()->with('status', 'Category created.');
    }

    /**
     * Show the specified resource.
     * @param int $id
     * @return Response
     */
    public function show($id)
    {
        return view('campaign::show');
    }

    /**
     * Show the form for editing the specified resource.
     * @param int $id
     * @return Response
     */
    public function edit($id)
    {
        $category = CampaignCategory::find($id);
        
        return view('modules.campaign.categories.edit', compact('category'));
    }

    /**
     * Update the specified resource in storage.
     * @param Request $request
     * @param int $id
     * @return Response
     */
    public function update(CampaignCategoryUpdateRequest $request, $id)
    {
        $category = CampaignCategory::find($id);
        $category->name = $request->name;
        
        $category->save();

        return back()->with('status', 'Category updated.');
    }

    /**
     * Remove the specified resource from storage.
     * @param int $id
     * @return Response
     */
    public function destroy($id)
    {
        $campaign = CampaignCategory::find($id);
        $campaign->delete();

        return back();    
    }
}
