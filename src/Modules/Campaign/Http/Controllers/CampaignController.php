<?php

namespace Modules\Campaign\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;

use Modules\Campaign\Entities\Campaign;
use Modules\Campaign\Entities\CampaignCategory;
use Modules\Agent\Entities\AgentCampaign;
use Modules\Campaign\Http\Requests\CampaignStoreRequest;
use Modules\Campaign\Http\Requests\CampaignUpdateRequest;

use Auth;

class CampaignController extends Controller
{
    /**
     * [getRecords Ajax function for retrieving users and optimizing load time of datatable]
     *
     * @param   Request  $request  [$request description]
     *
     * @return  [type]             [return description]
     */
    public function getCampaigns(Request $request)
    {
        $columns = array( 
            0 => 'name',
            1 => 'url',
            2 => 'category_id',
            3 => 'actions'
        );

        $totalData = Campaign::count();

        $totalFiltered = $totalData; 

        $limit = $request->input('length');
        $start = $request->input('start');
        $order = $columns[$request->input('order.0.column')];
        $dir = $request->input('order.0.dir');

        if(empty($request->input('search.value')))
        {            
            $campaigns = Campaign::offset($start)
                    ->limit($limit)
                    ->orderBy($order,$dir)
                    ->get();
        }
        else 
        {
            $search = $request->input('search.value'); 

            $campaigns =  Campaign::where('name', 'LIKE',"%{$search}%")
                        ->orWhere('url', 'LIKE',"%{$search}%")
                        ->orWhere('category_id', 'LIKE',"%{$search}%")
                        ->offset($start)
                        ->limit($limit)
                        ->orderBy($order,$dir)
                        ->get();

            $totalFiltered = Campaign::where('name', 'LIKE',"%{$search}%")
                        ->orWhere('url', 'LIKE',"%{$search}%")
                        ->orWhere('category_id', 'LIKE',"%{$search}%")
                        ->count();
        }

        $data = array();
        if(!empty($campaigns))
        {
            $category_name = null;
            foreach ($campaigns as $index => $row)
            {
                $category_name = CampaignCategory::find($row->category_id);
                $toDate = strtotime($row->created_at);
                $date = date('d M Y', $toDate);

                $nestedData['name'] = $row->name;
                $nestedData['url'] = $row->url;
                $nestedData['category'] = $category_name->name;
                $nestedData['actions'] = '<a class="btn btn-success btn-sm" href="'.route('campaign-management.edit-campaign', $row->id).'" role="button">Edit</a> 
                                        <a class="btn btn-danger btn-sm" href="'.route('campaign-management.destroy-campaign', $row->id).'" role="button">Delete</a>';
                                
                $data[] = $nestedData;
            }
        }

        $json_data = array(
            "draw"            => intval($request->input('draw')),  
            "recordsTotal"    => intval($totalData),  
            "recordsFiltered" => intval($totalFiltered), 
            "data"            => $data   
        );

        echo json_encode($json_data); 
    }
    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index()
    {
        return view('campaign::index');
    }

    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create()
    {
        $campaign_categories = CampaignCategory::all();
        return view('campaign::create', compact('campaign_categories'));
    }

    /**
     * Store a newly created resource in storage.
     * @param Request $request
     * @return Response
     */
    public function store(CampaignStoreRequest $request)
    {
        Campaign::create($request->all());

        return back()->with('status', 'Campaign created.');
    }

    /**
     * Show the specified resource.
     * @param int $id
     * @return Response
     */
    public function show($id)
    {
        return view('campaign::show');
    }

    /**
     * Show the form for editing the specified resource.
     * @param int $id
     * @return Response
     */
    public function edit($id)
    {
        $campaign = Campaign::find($id);
        $campaign_categories = CampaignCategory::all();
        
        return view('campaign::edit', compact('campaign', 'campaign_categories'));
    }

    /**
     * Update the specified resource in storage.
     * @param Request $request
     * @param int $id
     * @return Response
     */
    public function update(CampaignUpdateRequest $request, $id)
    {
        $campaign = Campaign::find($id);
        $campaign->name = $request->name;
        $campaign->url = $request->url;
        $campaign->category_id = $request->category_id;
        
        $campaign->save();

        return back()->with('status', 'Campaign updated.');
    }

    /**
     * Remove the specified resource from storage.
     * @param int $id
     * @return Response
     */
    public function destroy($id)
    {
        $campaign = Campaign::find($id);
        $campaign->delete();

        return back();    
    }

    public function list()
    {
        $campaigns = Campaign::all();
        return view('campaign::list', compact('campaigns'));
    }

}
