<?php

namespace Modules\Campaign\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class CampaignUpdateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

     /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required|string|max:50',
            'url' => ['required', 'url', \Illuminate\Validation\Rule::unique('campaigns')->ignore($this->id)],
            'category_id' => 'required'
        ];
    }

    public function messages()
    {
        return [
            'name.required' => 'Campaign name is required!',
            'url.required' => 'Campaign url is required!',
            'category_id.required' => 'Category is required!'
        ];
    }
}
