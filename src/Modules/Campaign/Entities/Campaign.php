<?php

namespace Modules\Campaign\Entities;

use Illuminate\Database\Eloquent\Model;
use GoldSpecDigital\LaravelEloquentUUID\Database\Eloquent\Uuid;

class Campaign extends Model
{
    use Uuid;
    
    /**
     * The "type" of the auto-incrementing ID.
     *
     * @var string
     */
    protected $keyType = 'string';

    /**
     * Indicates if the IDs are auto-incrementing.
     *
     * @var bool
     */
    public $incrementing = false;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $guarded = [];

    public function category()
    {
        return $this->belongsTo('Modules\Campaign\Entities\CampaignCategory', 'category_id');
    }

    public function agents()
    {
        return $this->hasMany('Modules\Agent\Entities\AgentCampaign', 'campaign_id');
    }

    public function payments()
    {
        return $this->hasMany('Modules\Payment\Entities\Payment', 'campaign_id');
    }
}
