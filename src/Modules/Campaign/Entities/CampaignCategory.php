<?php

namespace Modules\Campaign\Entities;

use Illuminate\Database\Eloquent\Model;
use GoldSpecDigital\LaravelEloquentUUID\Database\Eloquent\Uuid;

class CampaignCategory extends Model
{    
    use Uuid;
    protected $table = 'campaign_categories';
    /**
     * The "type" of the auto-incrementing ID.
     *
     * @var string
     */
    protected $keyType = 'string';

    /**
     * Indicates if the IDs are auto-incrementing.
     *
     * @var bool
     */
    public $incrementing = false;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $guarded = [];
    // protected $fillable = [];

    public function campaigns()
    {
        return $this->hasMany('Modules\Campaign\Entities\Campaign');
    }
}
