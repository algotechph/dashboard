<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::group(['middleware' => 'api', 'prefix' => 'campaign-management'], function () {
    Route::post('/campaigns', 'CampaignController@getCampaigns')->name('ajax.campaign-management');
    Route::post('/campaign-categories', 'CampaignCategoryController@getCampaignCategory')->name('ajax.campaign-management-category');
});