<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::prefix('campaign-management')->middleware('auth')->group(function() {
    Route::get('/', 'CampaignController@index')->name('campaign-management.index');
    Route::get('/campaign/create', 'CampaignController@create')->name('campaign-management.create-campaign');
    Route::post('/campaign/create', 'CampaignController@store')->name('campaign-management.store-campaign');
    Route::get('/campaign/edit/{campaign_id}', 'CampaignController@edit')->name('campaign-management.edit-campaign');
    Route::post('/campaign/update/{campaign_id}', 'CampaignController@update')->name('campaign-management.update-campaign');
    Route::get('/campaign/delete/{campaign_id}', 'CampaignController@destroy')->name('campaign-management.destroy-campaign');

    /** CATEGORY */
    Route::get('/category', 'CampaignCategoryController@index')->name('campaign-management.category-index');
    Route::get('/campaign/category/create', 'CampaignCategoryController@create')->name('campaign-management.create-campaign-category');
    Route::post('/campaign/category/create', 'CampaignCategoryController@store')->name('campaign-management.store-campaign-category');
    Route::get('/campaign/category/edit/{category_id}', 'CampaignCategoryController@edit')->name('campaign-management.edit-campaign-category');
    Route::post('/campaign/category/update/{category_id}', 'CampaignCategoryController@update')->name('campaign-management.update-campaign-category');
    Route::get('/campaign/category/delete/{category_id}', 'CampaignCategoryController@destroy')->name('campaign-management.destroy-campaign-category');

});

Route::group(['middleware' => ['web', 'auth']], function (){
    Route::get('/campaign/list', 'CampaignController@list')->name('campaign-management.list');
});