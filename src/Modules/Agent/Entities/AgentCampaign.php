<?php
namespace Modules\Agent\Entities;

use Illuminate\Database\Eloquent\Model;
use GoldSpecDigital\LaravelEloquentUUID\Database\Eloquent\Uuid;

class AgentCampaign extends Model
{
    use Uuid;
    protected $table = 'agent_campaigns';
    public $timestamps = true;
    protected $fillable = ['user_id', 'campaign_id', 'views'];
    protected $keyType = 'string';

    public function campaign()
    {
        return $this->belongsTo('Modules\Campaign\Entities\Campaign', 'campaign_id');
    }

    public function agent()
    {
        return $this->belongsTo('Modules\Agent\Entities\Agent', 'user_id');
    }

    public function campaigns()
    {
        return $this->hasMany('Modules\Campaign\Entities\Campaign', 'campaign_id');
    }

    public function user()
    {
        return $this->belongsTo('App\User', 'user_id');
    }

    public function payments()
    {
        return $this->hasMany('Modules\Payment\Entities\Payment', 'user_id', 'user_id');
    }
}
