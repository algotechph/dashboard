<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::prefix('agent')->group(function() {
    Route::get('/', 'AgentController@index');
});

Route::prefix('agent')->middleware('auth')->group(function() {
    Route::get('/campaign/user/{user_id}', 'AgentController@myCampaign')->name('agent-management.my-list');
    Route::get('/campaign/user/destroy/{id}', 'AgentController@destroyMyCampaign')->name('agent-management.destroy-user-campaign');
    //Route::get('/campaign/use/{user_id}/{campaign_id}', 'AgentController@useCampaign')->name('agent-management.use-campaign');
    
    Route::post('/campaign/use/campaign', 'AgentController@enableMyCampaign')->name('ajax.agent-management.use-campaign');
    Route::post('campaign/remove/my-campaign', 'AgentController@disableMyCampaign')->name('ajax.agent-management.remove-my-campaign');
});