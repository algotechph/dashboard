<?php

namespace Modules\Agent\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Modules\Campaign\Entities\Campaign;
use Modules\Agent\Entities\AgentCampaign;
use App\User;
use Carbon\Carbon;

class AgentController extends Controller
{
    public function countSubscription(Request $request)
    {
        try{
            $agent = User::find($request->agent_id);
            $user_campaign = AgentCampaign::where('user_id', $agent->id)->where('campaign_id', $request->campaign_id)->first();

            if(isset($user_campaign))
            {
                $user_campaign->subscriptions = $user_campaign->subscriptions + 1;
                $user_campaign->save();
            }

            return response()->json([
                'status' => 1,
                'message' => 'Subscription updated.',
            ], 200); 
        
        } catch (Exception $e) {
            return response()->json([
                'status' => 0,
                'message' => 'Something went wrong.',
                'error' => $e->getMessage()
            ], 400); 
        }
    }

    public function countView($aid, $cid)
    {
        try{
            $agent = User::find($aid);
            $user_campaign = AgentCampaign::where('user_id', $agent->id)->where('campaign_id', $cid)->first();

            if(isset($user_campaign))
            {
                $startDate = Carbon::now(); //returns current day
                $firstDay = $startDate->firstOfMonth(); 
                if($firstDay->day == 01)
                {
                    if($user_campaign->created_at->format('d') <> 01)
                    {
                        $user_campaign->created_at = $startDate;
                        $user_campaign->views = 0;
                    }
                    
                }
                $user_campaign->views = $user_campaign->views + 1;
                $user_campaign->save();
            }

            return response()->json([
                'status' => 1,
                'message' => 'Views updated.',
            ], 200); 

        } catch (Exception $e) {
            
            return response()->json([
                'status' => 0,
                'message' => 'Something went wrong.',
                'error' => $e->getMessage()
            ], 400); 
        }
        
    }

    // public function useCampaign($user_id, $campaign_id)
    // {
    //     $agent_campaign = new AgentCampaign();
    //     $campaign = Campaign::find($campaign_id);
    //     $agent_campaign->user_id = $user_id;
    //     $agent_campaign->campaign_id = $campaign->id;
    //     $agent_campaign->campaign_url = $campaign->url .'/registration/?aid='. $user_id. '&cid='. $campaign_id;
    //     $agent_campaign->subscriptions = 0;
    //     $agent_campaign->views = 0;
    //     $agent_campaign->save();

    //     return back()->with('status', 'Campaign used.');
    // }

    public function myCampaign($id)
    {
        $agent_campaigns = AgentCampaign::where('user_id', $id)->get();

        return view('campaign::user_campaign', compact('agent_campaigns'));
    }

    public function enableMyCampaign(Request $request)
    {
        try{
            $agent_campaign = new AgentCampaign();
            $campaign = Campaign::find($request->campaign_id);
            $agent_campaign->user_id = $request->user_id;
            $agent_campaign->campaign_id = $campaign->id;
            $agent_campaign->views = 0;
            $agent_campaign->save();

            return response()->json([
                'status' => 0,
                'message' => 'Campaign used.',
            ], 200); 
            
        } catch (Exception $e) {
            return response()->json([
                'status' => 0,
                'message' => 'Something went wrong.',
                'error' => $e->getMessage()
            ], 400); 
        }
    }

    public function disableMyCampaign(Request $request)
    {
        try{
            $agent_campaign = AgentCampaign::find($request->id);
            $agent_campaign->delete();
           
            return response()->json([
                'status' => 0,
                'message' => 'Campaign removed.',
            ], 200); 

        } catch(Exception $e){
            return response()->json([
                'status' => 0,
                'message' => 'Something went wrong.',
                'error' => $e->getMessage()
            ], 400); 
        }
    }

    public function destroyMyCampaign($id)
    {
        try{
            $agent_campaign = AgentCampaign::find($id);
            if($agent_campaign)
            {
                $agent_campaign->delete();
            }            
           
            return back()->with('status', 'Campaign removed');

        } catch(Exception $e){
            return back()->with('status', 'Campaign remove failed');
        }
    }

}
