<div class="row">

    @php        
        $agent_subscriptions_today = 0;
        $agent_month_subscriptions = 0;
        if($analytics['agent_subscriptions_today'] <> null)
        {
            $_agent_subscriptions_today = json_decode($analytics['agent_subscriptions_today']);
            $agent_subscriptions_today = $_agent_subscriptions_today[0]->total_subscriptions;
        }
        if($analytics['agent_month_subscriptions'] <> null)
        {
            $_agent_month_subscriptions = json_decode($analytics['agent_month_subscriptions']);
            foreach($_agent_month_subscriptions as $row)
            {
                $agent_month_subscriptions += $row->total_subscriptions;
            }
        }
    @endphp

    <!-- Campaign Views -->
    <div class="col-xl-4 col-md-12 mb-4">
        <div class="card border-left-primary shadow h-100 py-2">
            <div class="card-body">
                <div class="row no-gutters align-items-center">
                    <div class="col mr-2">
                        <div class="text-xs font-weight-bold text-primary text-uppercase mb-1">Campaign Views ({{\Carbon\Carbon::now()->format('F')}})</div>
                        <div class="h5 mb-0 font-weight-bold text-gray-800">{{ $analytics['agent_campaign_views_month'] }}</div>
                    </div>
                    <div class="col-auto">
                        <i class="fas fa-calendar fa-2x text-gray-300"></i>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="col-xl-4 col-md-12 mb-4">
        <div class="card border-left-info shadow h-100 py-2">
            <div class="card-body">
                <div class="row no-gutters align-items-center">
                    <div class="col mr-2">
                        <div class="text-xs font-weight-bold text-info text-uppercase mb-1">Subscriptions ({{\Carbon\Carbon::now()->format('F')}})</div>
                        <div class="h5 mb-0 font-weight-bold text-gray-800">{{ $agent_month_subscriptions }}</div>
                    </div>
                    <div class="col-auto">
                        <i class="fas fa-users fa-2x text-gray-300"></i>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- Subscriptions -->
    <div class="col-xl-4 col-md-12 mb-4">
        <div class="card border-left-success shadow h-100 py-2">
            <div class="card-body">
                <div class="row no-gutters align-items-center">
                    <div class="col mr-2">
                        <div class="text-xs font-weight-bold text-success text-uppercase mb-1">Subscriptions (today)</div>
                        <div class="h5 mb-0 font-weight-bold text-gray-800">{{ $agent_subscriptions_today }}</div>
                    </div>
                    <div class="col-auto">
                        <i class="fas fa-user fa-2x text-gray-300"></i>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="row">

    @include('dashboard.includes.table')

</div>