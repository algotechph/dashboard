<div class="row">

    @php 
        $subscriptions_today = 0;    
        $earnings_today = 0;    
        $earnings_current_month = 0;
        if($analytics['earnings_today'] <> null)
        {
            $_earnings_today = json_decode($analytics['earnings_today']);
            $earnings_today = $_earnings_today[0]->total_amount;
            $subscriptions_today = $_earnings_today[0]->total_leads;
        }

        if(($analytics['earnings_month']) != null)
        {
            $_earnings_current_month = json_decode($analytics['earnings_month']);
            $earnings_current_month = number_format($_earnings_current_month[0]->total_amount, 2);
        }
    @endphp

    <!-- Earnings (Monthly) -->
    <div class="col-xl-3 col-md-6 mb-4">
        <div class="card border-left-primary shadow h-100 py-2">
            <div class="card-body">
                <div class="row no-gutters align-items-center">
                    <div class="col mr-2">
                        <div class="text-xs font-weight-bold text-primary text-uppercase mb-1">Earnings ({{\Carbon\Carbon::now()->format('F')}})</div>
                        <div class="h5 mb-0 font-weight-bold text-gray-800">${{$earnings_current_month}}</div>
                    </div>
                    <div class="col-auto">
                        <i class="fas fa-calendar fa-2x text-gray-300"></i>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- Earnings (Today) -->
    <div class="col-xl-3 col-md-6 mb-4">
        <div class="card border-left-success shadow h-100 py-2">
            <div class="card-body">
                <div class="row no-gutters align-items-center">
                    <div class="col mr-2">
                        <div class="text-xs font-weight-bold text-success text-uppercase mb-1">Earnings (Today)</div>
                        <div class="h5 mb-0 font-weight-bold text-gray-800">${{$earnings_today}}</div>
                    </div>
                    <div class="col-auto">
                        <i class="fas fa-dollar-sign fa-2x text-gray-300"></i>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- Campaign Views -->
    <div class="col-xl-3 col-md-6 mb-4">
        <div class="card border-left-info shadow h-100 py-2">
            <div class="card-body">
                <div class="row no-gutters align-items-center">
                    <div class="col mr-2">
                        <div class="text-xs font-weight-bold text-info text-uppercase mb-1">Campaign Views</div>
                        <div class="h5 mb-0 font-weight-bold text-gray-800">{{ $analytics['campaign_views'] }}</div>
                    </div>
                    <div class="col-auto">
                        <i class="fas fa-clipboard-list fa-2x text-gray-300"></i>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- Subscriptions (today) -->
    <div class="col-xl-3 col-md-6 mb-4">
        <div class="card border-left-warning shadow h-100 py-2">
            <div class="card-body">
                <div class="row no-gutters align-items-center">
                    <div class="col mr-2">
                        <div class="text-xs font-weight-bold text-warning text-uppercase mb-1">Subscriptions (today)</div>
                        <div class="h5 mb-0 font-weight-bold text-gray-800">{{ $subscriptions_today }}</div>
                    </div>
                    <div class="col-auto">
                        <i class="fas fa-users fa-2x text-gray-300"></i>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="row">
    <line-chart-analytics :chart_data='{{ json_encode($analytics) }}'></line-chart-analytics>
    <!-- Line Chart -->
    
    <!-- Pie Chart -->
    <pie-chart-analytics :chart_data='{{ json_encode($analytics) }}'></pie-chart-analytics>
    
</div>

<div class="row">
    @include('dashboard.includes.table')
</div>