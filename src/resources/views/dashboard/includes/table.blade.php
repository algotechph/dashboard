<!-- Content Column -->
<div class="col-lg-12 mb-4">

    <!-- Project Card Example -->
    <div class="card shadow mb-4">
        <div class="card-header py-3">
            <h6 class="m-0 font-weight-bold text-primary">{{ (Auth::user()->type == 1? 'My Campaigns' : 'Campaign Analytics')}}</h6>
        </div>
        <div class="card-body">

            <div class="row">
                <div class="col-lg-12">
                    
                        <table class="table m-b-0 table-hover table-striped table-bordered dt-responsive nowrap" id="tbl-campaigns">
                            <thead>
                                <tr>
                                    @if(Auth::user()->type == 2)
                                        <th>Agent</th> 
                                    @endif 
                                    <th>Campaign</th>                                
                                    <th>URL</th>                             
                                    <th>Views</th>                         
                                    <th>Subscriptions</th> 
                                    <th>Conversion</th>                                    
                                    @if(Auth::user()->type == 1)
                                        <th>Actions</th> 
                                    @endif
                                </tr>
                            </thead>
                            <tbody>
                                @if(Auth::user()->type == 1)
                                    @foreach($campaigns as $campaign)
                                        @php $subscription = 0; @endphp 
                                        @php
                                            $affiliate_link = $campaign->campaign->url . '/registration/?aid=' . Auth::user()->id . '&cid='. $campaign->campaign->id;
                                        @endphp                               
                                        <tr>
                                            <td>{{$campaign->campaign->name}}</td>
                                            <td>{{\Illuminate\Support\Str::limit($affiliate_link, 100)}}</td>
                                            <td>{{$campaign->views}}</td>
                                            <td>
                                                @foreach($campaign->payments as $payment)
                                                    @if($payment->created_at->format('m') == \Carbon\Carbon::now()->format('m'))
                                                        @if($payment->campaign_id == $campaign->campaign_id)
                                                            @php $subscription++ @endphp
                                                        @endif                                            
                                                    @endif                                            
                                                @endforeach
                                                {{ $subscription }}
                                            </td>  
                                            <td>
                                                @if($subscription && $campaign->views <> 0)
                                                    @php $percent = ($subscription / $campaign->views) * 100  @endphp
                                                    {{ round($percent, 2) .'%'}}
                                                @else
                                                    0%
                                                @endif
                                            </td>  
                                            <td>
                                                <button class="btn btn-success btn-sm clipboard" data-clipboard-text="{{ $affiliate_link }}" data-message="Copied!" data-original="" data-toggle="tooltip" data-placement="bottom" title="Copy URL to clipboard">
                                                    <i class="fa fa-clipboard"></i>
                                                </button>
                                                <a class="btn btn-danger btn-sm clipboard" 
                                                    data-toggle="tooltip" 
                                                    data-placement="bottom" 
                                                    title="Remove campaign" 
                                                    role="button" 
                                                    href="{{route('agent-management.destroy-user-campaign', $campaign->id)}}" 
                                                    role="button">
                                                        <i class="fa fa-trash"></i>
                                                </a>
                                            </td>                                        
                                        </tr>
                                    @endforeach
                                @elseif(Auth::user()->type == 2)
                                    @foreach($campaigns as $campaign)
                                        @if($campaign->user->manager == Auth::user()->id)
                                            @php 
                                                $subscription = 0;            
                                            @endphp                                
                                            <tr>
                                            <td>{{ $campaign->user->getFullNameAttribute() }}</td>
                                                <td>{{$campaign->campaign->name}}</td>
                                                <td>{{$campaign->campaign->url}}</td>
                                                <td>{{ $campaign->views }}</td>
                                                <td>
                                                    @foreach($campaign->payments as $payment)
                                                        @if($payment->created_at->format('m') == \Carbon\Carbon::now()->format('m'))
                                                            @if($payment->campaign_id == $campaign->campaign_id)
                                                                @php $subscription++ @endphp
                                                            @endif                                            
                                                        @endif                                            
                                                    @endforeach
                                                    {{ $subscription }}
                                                </td>                                                 
                                                
                                                <td>                                                
                                                    @if($subscription && $campaign->views <> 0)
                                                        @php $percent = ($subscription / $campaign->views) * 100  @endphp
                                                        {{ round($percent, 2) .'%'}}
                                                    @else
                                                        0%
                                                    @endif
                                                </td>                                          
                                            </tr>
                                        @endif
                                    
                                    @endforeach 
                                        
                                @else
                                    @foreach($campaigns as $campaign)
                                        @php 
                                            $subscription = array();
                                            $views = array();         
                                        @endphp                                
                                        <tr>
                                            <td>{{$campaign->name}}</td>
                                            <td>{{$campaign->url}}</td>
                                            <td>
                                                @foreach($campaign->agents as $agent)
                                                    @if($agent->campaign_id == $campaign->id)
                                                        @php $views[] = $agent->views @endphp
                                                    @endif                                            
                                                @endforeach
                                                {{ array_sum($views) }}
                                            </td>
                                            <td>
                                                @foreach($campaign->payments as $payment)
                                                    @if($payment->created_at->format('m') == \Carbon\Carbon::now()->format('m'))
                                                        @if($payment->campaign_id == $campaign->id)
                                                            @php $subscription[] = $payment @endphp
                                                        @endif                                            
                                                    @endif                                            
                                                @endforeach
                                                {{ count($subscription) }}
                                            </td>  
                                            <td>                                                
                                                @if($subscription)
                                                    @php $views = array() @endphp
                                                        @foreach($campaign->agents as $agent)                                                        
                                                            @php $percent = 0 @endphp
                                                            @if($agent->campaign_id == $campaign->id)
                                                                @php $views[] = $agent->views @endphp  
                                                            @endif                                              
                                                        @endforeach
                                                        @if(array_sum($views) && count($subscription) <> 0)
                                                            @php $percent = (count($subscription) / array_sum($views)) * 100  @endphp
                                                            {{ round($percent, 2) .'%'}}
                                                        @endif
                                                @else
                                                    0%
                                                 @endif
                                            </td>                                          
                                        </tr>
                                    
                                    @endforeach 
                                @endif
                            </tbody>
                        </table> 
                </div>
            </div>
        </div>
    </div>

</div>