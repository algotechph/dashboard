<ul class="navbar-nav bg-gradient-primary sidebar sidebar-dark accordion" id="accordionSidebar">

        <!-- Sidebar - Brand -->
        <a class="sidebar-brand d-flex align-items-center justify-content-center" href="{{ route('dashboard') }}">
            <div class="sidebar-brand-icon rotate-n-15">
                {{-- <i class="fas fa-laugh-wink"></i> --}}
            </div>
            {{-- <div class="sidebar-brand-text mx-3"> {{Auth::user()->fullName}}</div> --}}
            <div class="sidebar-brand-text mx-3"> {{Auth::user()->last_name}}</div>
        </a>

        <!-- Divider -->
        <hr class="sidebar-divider my-0">

        <!-- Nav Item - Dashboard -->
        <li class="nav-item {{ Nav::isRoute('dashboard') }}">
            <a class="nav-link" href="{{ route('dashboard') }}">
                <i class="fas fa-fw fa-tachometer-alt"></i>
                <span>{{ __('Dashboard') }}</span></a>
        </li>

        <!-- Divider -->
        <hr class="sidebar-divider">

        <!-- Heading -->
        <div class="sidebar-heading">
            {{ __('Settings') }}
        </div>

        <!-- Nav Item - Profile -->
        <li class="nav-item {{ Nav::isRoute('profile') }}">
            <a class="nav-link" href="{{ route('profile') }}">
                <i class="fas fa-fw fa-user"></i>
                <span>{{ __('Profile') }}</span>
            </a>
        </li>


        <li class="nav-item {{ Nav::isRoute('campaign-management.list') }}">
            <a class="nav-link" href="{{ route('campaign-management.list') }}">
                <i class="fas fa-fw fa-hands-helping"></i>
                <span>{{ __('Campaigns') }}</span>
            </a>
        </li>

        <li class="nav-item {{ Nav::isRoute('agent-management.my-list') }}">
            <a class="nav-link" href="{{ route('agent-management.my-list', Auth::user()->id) }}">
                <i class="fas fa-fw fa-hands-helping"></i>
                <span>{{ __('My Campaigns') }}</span>
            </a>
        </li>



        
        <!-- Divider -->
        <hr class="sidebar-divider d-none d-md-block">
        @if(Auth::user()->type == 2)
            <div class="sidebar-heading">
                {{ __('Management') }}
            </div>
            <li class="nav-item {{ Nav::isRoute('user-management.index')}} {{Nav::isRoute('user-management.create-user')}} {{ Nav::isRoute('user-management.edit-user')}}" >
                <a class="nav-link" href="{{ route('user-management.index') }}">
                    <i class="fas fa-fw fa-users"></i>
                    <span>{{ __('User') }}</span>
                </a>
            </li>
        @endif
        {{-- ADMIN LINKS --}}
        @if(Auth::user()->type == 0)
            <div class="sidebar-heading">
                {{ __('Management') }}
            </div>

            <li class="nav-item {{ Nav::isRoute('user-management.index')}} {{Nav::isRoute('user-management.create-user')}} {{ Nav::isRoute('user-management.edit-user')}}" >
                <a class="nav-link" href="{{ route('user-management.index') }}">
                    <i class="fas fa-fw fa-users"></i>
                    <span>{{ __('User') }}</span>
                </a>
            </li>
            
            <li class="nav-item {{ Nav::isRoute('campaign-management.index') }} {{Nav::isRoute('campaign-management.create-campaign')}} {{ Nav::isRoute('campaign-management.edit-campaign')}}">
                <a class="nav-link" href="{{ route('campaign-management.index') }}">
                    <i class="fas fa-fw fa-hands-helping"></i>
                    <span>{{ __('Campaigns') }}</span>
                </a>
            </li>

            <li class="nav-item {{ Nav::isRoute('campaign-management.category-index') }} {{Nav::isRoute('campaign-management.create-campaign-category')}} {{ Nav::isRoute('campaign-management.edit-campaign-category')}}">
                <a class="nav-link" href="{{ route('campaign-management.category-index') }}">
                    <i class="fas fa-fw fa-cog"></i>
                    <span>{{ __('Campaign Category') }}</span>
                </a>
            </li>

            <li class="nav-item {{ Nav::isRoute('show.sales') }}">
                <a class="nav-link" href="{{ route('show.sales') }}">
                    <i class="fas fa-fw fa-dollar-sign"></i>
                    <span>{{ __('Sales') }}</span>
                </a>
            </li>
        
        @endif

        <!-- Sidebar Toggler (Sidebar) -->
        <div class="text-center d-none d-md-inline">
            <button class="rounded-circle border-0" id="sidebarToggle"></button>
        </div>

    </ul>