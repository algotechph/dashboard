@extends('layouts.admin')
@section('styles')
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.21/css/dataTables.bootstrap4.min.css">
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/responsive/2.2.5/css/responsive.bootstrap4.min.css">
@stop
@section('main-content')

    <!-- Page Heading -->
    <h1 class="h3 mb-4 text-gray-800">{{ __('Dashboard') }}</h1>

    @if(Auth::user()->type == 0)
        @include('dashboard.admin')
    @else
        @include('dashboard.agent')
    @endif
   
@endsection

@section('scripts')
    
    <script src="https://cdn.datatables.net/1.10.21/js/jquery.dataTables.min.js" defer></script>
    <script src="https://cdn.datatables.net/1.10.21/js/dataTables.bootstrap4.min.js" defer></script>
    <script src="https://cdn.datatables.net/responsive/2.2.5/js/dataTables.responsive.min.js" defer></script>
    <script src="https://cdn.datatables.net/responsive/2.2.5/js/responsive.bootstrap4.min.js" defer></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/clipboard.js/2.0.4/clipboard.min.js"></script>

    <script>
        $(function() {
            $('#tbl-campaigns').DataTable({responsive: true});
            
            $('[data-toggle="tooltip"]').tooltip();
            var clipboard = new ClipboardJS('.clipboard');

            clipboard.on('success', function(e) {
                var btn = $(e.trigger);
                setTooltip(btn,'Copied!');
                hideTooltip(btn);
            });

            clipboard.on('error', function(e) {
                var btn = $(e.trigger);
                setTooltip(btn,'Failed!');
                hideTooltip();
            });
            $('#tbl-campaigns').DataTable();
        });
        function setTooltip(btn, message) {
            btn.tooltip('hide')
            .attr('data-original-title', message)
            .tooltip('show');
        }

        function hideTooltip(btn) {
            setTimeout(function() {
                btn.tooltip('hide');
            }, 1000);
        }

    </script>
@stop
