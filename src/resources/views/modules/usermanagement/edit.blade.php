@extends('layouts.admin')

@section('main-content')
    <!-- Page Heading -->
    <h1 class="h3 mb-4 text-gray-800">{{ __('User Management') }}</h1>

    <div class="row justify-content-center">

        <div class="col-lg-12">

            <div class="card shadow mb-4">

                <div class="card-header">
                    @if (session('status'))
                        <div class="alert alert-success">
                            {{ session('status') }}
                        </div>
                    @endif
                    @if ($errors->any())
                        <div class="alert alert-danger">
                            <ul>
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div>
                    @endif
                    <h4>Edit User</h4>
                </div>

                <div class="card-body">

                    <div class="row">
                        <div class="col-lg-12">

                            <form method="POST" action="{{ route('user-management.update-user', $user->id) }}">
                                @csrf
                                <input class="form-control" name="id" type="hidden" value="{{ $user->id }}" >
                                <div class="form-group">
                                    <label for="exampleInputEmail1">Account Type</label>
                                    <select name="type" required class="form-control">
                                        <option value="0" {{$user->type == 0 ? 'selected' : ''}}>Admin</option>
                                        <option value="1" {{$user->type == 1 ? 'selected' : ''}}>Agent</option>
                                        <option value="2" {{$user->type == 2 ? 'selected' : ''}}>Manager</option>
                                      </select>
                                </div>
                                                
                                <div class="form-group">
                                    <label for="exampleInputEmail1">First name</label>
                                <input type="text" name="first_name" class="form-control" id="exampleInputEmail1" required placeholder="First name" value="{{ $user->first_name }}">
                                </div>

                                <div class="form-group">
                                    <label for="exampleInputEmail1">Last name</label>
                                    <input type="text" name="last_name" class="form-control" id="exampleInputEmail1" required placeholder="Last name" value="{{ $user->last_name }}">
                                </div>

                                <div class="form-group">
                                    <label for="exampleInputEmail1">Email address</label>
                                    <input type="email" name="email" class="form-control" id="exampleInputEmail1" required placeholder="Email" value="{{ $user->email }}">
                                </div>

                                <div class="form-group">
                                    <label for="exampleInputEmail1">Password</label>
                                    <input type="password" name="password" minlength="6" class="form-control" id="exampleInputEmail1" required placeholder="Password" value="{{ $user->password }}">
                                </div>
                                
                                <div class="form-group">
                                    <button type="submit" class="btn btn-primary">Update</button>
                                </div>
                            </form>
                            
                        </div>
                    </div>

                </div>
            </div>

        </div>

    </div>

@endsection

