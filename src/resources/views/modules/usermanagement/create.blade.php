@extends('layouts.admin')

@section('main-content')
    <!-- Page Heading -->
    <h1 class="h3 mb-4 text-gray-800">{{ __('User Management') }}</h1>

    <div class="row justify-content-center">

        <div class="col-lg-12">

            <div class="card shadow mb-4">

                <div class="card-header">
                    @if (session('status'))
                        <div class="alert alert-success">
                            {{ session('status') }}
                        </div>
                    @endif
                    @if ($errors->any())
                        <div class="alert alert-danger">
                            <ul>
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div>
                    @endif
                    <h4>Create User</h4>
                </div>

                <div class="card-body">

                    <div class="row">
                        <div class="col-lg-12">

                            <form method="POST" action="{{ route('user-management.store-user') }}">
                                @csrf                                
                                <div class="form-group">
                                    <label for="exampleInputEmail1">Account Type</label>
                                    @if(Auth::user()->type == 0) 
                                        <select name="type" required class="form-control">
                                            <option value="0">Admin</option>
                                            <option value="1" selected>Agent</option>
                                            <option value="2">Manager</option>
                                        </select>
                                    @else
                                        <input type="hidden" name="manager" value="{{ Auth::user()->id }}">
                                        <select name="type" required class="form-control">
                                            <option value="1" selected>Agent</option>
                                        </select>
                                    @endif
                                </div>
                                
                                <div class="form-group">
                                    <label for="exampleInputEmail1">First name</label>
                                    <input type="text" name="first_name" class="form-control" id="exampleInputEmail1" required placeholder="First name">
                                </div>

                                <div class="form-group">
                                    <label for="exampleInputEmail1">Last name</label>
                                    <input type="text" name="last_name" class="form-control" id="exampleInputEmail1" required placeholder="Last name">
                                </div>

                                <div class="form-group">
                                    <label for="exampleInputEmail1">Email address</label>
                                    <input type="email" name="email" class="form-control" id="exampleInputEmail1" required placeholder="Email">
                                </div>

                                <div class="form-group">
                                    <label for="exampleInputEmail1">Password</label>
                                    <input type="password" minlength="6" name="password" class="form-control" id="exampleInputEmail1" required placeholder="Password">
                                </div>
                                
                                <div class="form-group">
                                    <button type="submit" class="btn btn-primary">Create</button>
                                </div>
                            </form>
                            
                        </div>
                    </div>

                </div>
            </div>

        </div>

    </div>

@endsection

