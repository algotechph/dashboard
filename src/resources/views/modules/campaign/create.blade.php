@extends('layouts.admin')

@section('main-content')
    <!-- Page Heading -->
    <h1 class="h3 mb-4 text-gray-800">{{ __('Campaign Management') }}</h1>

    <div class="row justify-content-center">

        <div class="col-lg-12">

            <div class="card shadow mb-4">

                <div class="card-header">
                    @if (session('status'))
                        <div class="alert alert-success">
                            {{ session('status') }}
                        </div>
                    @endif
                    @if ($errors->any())
                        <div class="alert alert-danger">
                            <ul>
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div>
                    @endif
                    <h4>Create Campaign</h4>
                </div>

                <div class="card-body">

                    <div class="row">
                        <div class="col-lg-12">

                            <form method="POST" action="{{ route('campaign-management.store-campaign') }}">
                                @csrf

                                <div class="form-group">
                                    <label for="exampleInputEmail1">Category</label>
                                    <select name="category_id" required class="form-control">
                                        @foreach($campaign_categories as $category)
                                            <option value="{{ $category->id }}">{{ $category->name }}</option>
                                        @endforeach
                                    </select>
                                </div>

                                <div class="form-group">
                                    <label for="exampleInputEmail1">Campaign name</label>
                                    <input type="text" name="name" class="form-control" id="exampleInputEmail1" required placeholder="Campaign name">
                                </div>

                                <div class="form-group">
                                    <label for="exampleInputEmail1">URL</label>
                                    <input type="url" name="url" class="form-control" id="exampleInputEmail1" required placeholder="Url">
                                </div>
                                
                                <div class="form-group">
                                    <button type="submit" class="btn btn-primary">Create</button>
                                </div>
                            </form>
                            
                        </div>
                    </div>

                </div>
            </div>

        </div>

    </div>

@endsection

