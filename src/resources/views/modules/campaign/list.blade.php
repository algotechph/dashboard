@extends('layouts.admin')
@section('styles')
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.21/css/dataTables.bootstrap4.min.css">
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/responsive/2.2.5/css/responsive.bootstrap4.min.css">
<link href="https://gitcdn.github.io/bootstrap-toggle/2.2.2/css/bootstrap-toggle.min.css" rel="stylesheet">
<link href="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.css" rel="stylesheet">
<style>
    .toggle.ios, .toggle-on.ios, .toggle-off.ios { border-radius: 20px !important; }
    .toggle.ios .toggle-handle { border-radius: 20px !important; }
</style>
@stop

@section('main-content')
    <!-- Page Heading -->
    <h1 class="h3 mb-4 text-gray-800">{{ __('Campaigns') }}</h1>

    <div class="row justify-content-center">

        <div class="col-lg-12">

            <div class="card shadow mb-4">

                <div class="card-body">

                    <div class="row">
                        <div class="col-lg-12">

                            <table class="table m-b-0 table-hover table-striped table-bordered dt-responsive nowrap" id="tbl-campaigns">
                                <thead>
                                    <tr>
                                        <th>Campaign</th>                                
                                        <th>URL</th>                             
                                        <th>Category</th>                         
                                        <th>Actions</th> 
                                    </tr>
                                </thead>
                                <tbody>
                                   
                                    @foreach($campaigns as $campaign)
                                       @php
                                            $hide = false;                                                                                       
                                       @endphp
                                    
                                        <tr>
                                            <td>{{$campaign->name}}</td>
                                            <td>{{$campaign->url}}</td>
                                            <td>{{$campaign->category->name}}</td>
                                            <td> 
                                               
                                                @foreach($campaign->agents as $row)
                                                    @if(Auth::user()->id == $row->user_id)
                                                        @php $hide = true; @endphp
                                                        <input type="checkbox" data-on="Enabled" data-off="Disabled" class="my-campaign" agentcampaign-id="{{$row->id}}" user-id="{{Auth::user()->id}}" campaign-id="{{$campaign->id}}" data-toggle="toggle" data-onstyle="success" data-size="small" data-offstyle="info" data-style="ios" checked>
                                                        {{-- <a href="#">
                                                            <button class="btn btn-success btn-sm" disabled>
                                                                Use campaign
                                                            </button>                                                            
                                                        </a>  --}}
                                                    @endif  
                                                
                                                @endforeach

                                                @if(!$hide)
                                                    {{-- <a class="btn btn-success btn-sm" href="{{ route('agent-management.use-campaign', array(Auth::user()->id, $campaign->id))}}" role="button">
                                                        Use campaign
                                                    </a> --}}
                                                    <input type="checkbox" data-on="Enabled" data-off="Disabled" class="my-campaign" user-id="{{Auth::user()->id}}" campaign-id="{{$campaign->id}}" data-toggle="toggle" data-onstyle="success" data-size="small" data-offstyle="info" data-style="ios">
                                                @endif
                                            </td>
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>     
                        </div>
                    </div>

                </div>
            </div>

        </div>

    </div>

@endsection

@section('scripts')
    <script src="https://cdn.datatables.net/1.10.21/js/jquery.dataTables.min.js"></script>
    <script src="https://cdn.datatables.net/1.10.21/js/dataTables.bootstrap4.min.js"></script>
    <script src="https://cdn.datatables.net/responsive/2.2.5/js/dataTables.responsive.min.js"></script>
    <script src="https://cdn.datatables.net/responsive/2.2.5/js/responsive.bootstrap4.min.js"></script>
    <script src="https://gitcdn.github.io/bootstrap-toggle/2.2.2/js/bootstrap-toggle.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.js"></script>

    <script>
        $('#tbl-campaigns').DataTable();
        $(function() {
            toastr.options = {
                "closeButton": true,
                "debug": false,
                "newestOnTop": false,
                "progressBar": false,
                "positionClass": "toast-top-right",
                "preventDuplicates": false,
                "onclick": null,
                "showDuration": "500",
                "hideDuration": "1000",
                "timeOut": "3000",
                "extendedTimeOut": "1000",
                "showEasing": "swing",
                "hideEasing": "linear",
                "showMethod": "fadeIn",
                "hideMethod": "fadeOut"
            }
            $('.my-campaign').change(function() {
                let user_id = $(this).attr('user-id');
                let campaign_id = $(this).attr('campaign-id');
                let id = $(this).attr('agentcampaign-id');
                
                if($(this).prop('checked')){
                    $.ajax({
                        url: "{{ route('ajax.agent-management.use-campaign') }}",
                        type: 'POST',
                        data: {
                            user_id: user_id, 
                            campaign_id: campaign_id, 
                            _token: "{{csrf_token()}}",
                            },
                        success: function(response) {
                            toastr["success"]("Campaign enabled! Check your My Campaigns page.")
                        }            
                    });
                } else {
                    $.ajax({
                        url: "{{ route('ajax.agent-management.remove-my-campaign') }}",
                        type: 'POST',
                        data: {
                            id: id, 
                            _token: "{{csrf_token()}}",
                            },
                        success: function(response) {
                            toastr["success"]("Campaign disabled! Check your My Campaigns page.")
                        }            
                    });
                    
                }
               
            })
        })
    </script>
@stop
