@extends('layouts.admin')
@section('styles')
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.21/css/dataTables.bootstrap4.min.css">
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/responsive/2.2.5/css/responsive.bootstrap4.min.css">
@stop

@section('main-content')
    <!-- Page Heading -->
    <h1 class="h3 mb-4 text-gray-800">{{ __('Campaign Management') }}</h1>

    <div class="row justify-content-center">

        <div class="col-lg-12">

            <div class="card shadow mb-4">

                <div class="card-header">
                    <a href="{{ route('campaign-management.create-campaign') }}" class="btn btn btn-outline-primary" role="button">Create Campaign</a>
                </div>

                <div class="card-body">

                    <div class="row">
                        <div class="col-lg-12">
                            <table class="table m-b-0 table-hover table-striped table-bordered dt-responsive nowrap" id="tbl-campaigns">
                                <thead>
                                    <tr>
                                        <th>Campaign</th>                                
                                        <th>URL</th>                             
                                        <th>Category</th>                         
                                        <th>Actions</th> 
                                    </tr>
                                </thead>
                            </table>     
                        </div>
                    </div>

                </div>
            </div>

        </div>

    </div>

@endsection

@section('scripts')
    <script src="https://cdn.datatables.net/1.10.21/js/jquery.dataTables.min.js"></script>
    <script src="https://cdn.datatables.net/1.10.21/js/dataTables.bootstrap4.min.js"></script>
    <script src="https://cdn.datatables.net/responsive/2.2.5/js/dataTables.responsive.min.js"></script>
    <script src="https://cdn.datatables.net/responsive/2.2.5/js/responsive.bootstrap4.min.js"></script>

    <script>

        $('#tbl-campaigns').DataTable( {
            "language": {"searchPlaceholder": "Filter Results"},
            "processing": true,
            "serverSide": true,
            "ajax":{
                    "url": "{{ route('ajax.campaign-management') }}",
                    "dataType": "json",
                    "type": "POST",
                    "data":{ _token: "{{csrf_token()}}"}
                },
            "columns": [
                { "data": "name" },
                { "data": "url" },   
                { "data": "category" },
                { "data": "actions" },  
            ] 
        });
    </script>
@stop
