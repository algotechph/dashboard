@extends('layouts.admin')
@section('styles')
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.21/css/dataTables.bootstrap4.min.css">
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/responsive/2.2.5/css/responsive.bootstrap4.min.css">
<style>
.card-header{
    border-bottom: none !important;
}
</style>
@stop

@section('main-content')
    <!-- Page Heading -->
    <h1 class="h3 mb-4 text-gray-800">{{ __('My Campaigns') }}</h1>

    <div class="row justify-content-center">

        <div class="col-lg-12">

            <div class="card shadow mb-4">

                @if (session('status') || $errors->any())
                    <div class="card-header">
                        @if (session('status'))
                            <div class="alert alert-success">
                                {{ session('status') }}
                            </div>
                        @endif
                        @if ($errors->any())
                            <div class="alert alert-danger">
                                <ul>
                                    @foreach ($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            </div>
                        @endif
                    
                    </div>
                @endif

                <div class="card-body">

                    <div class="row">
                        <div class="col-lg-12">
                            <table class="table m-b-0 table-hover table-striped table-bordered dt-responsive nowrap" id="tbl-campaigns">
                                <thead>
                                    <tr>
                                        <th>Campaign</th>                                
                                        <th>URL</th>                             
                                        <th>Category</th>                         
                                        <th>Actions</th> 
                                    </tr>
                                </thead>
                                <tbody>

                                    @foreach($agent_campaigns as $campaign)
                                        @php
                                            $affiliate_link = $campaign->campaign->url . '/registration/?aid=' . Auth::user()->id . '&cid='. $campaign->campaign->id;
                                        @endphp
                                        <tr>
                                            <td>{{$campaign->campaign->name}}</td>
                                            <td>{{$affiliate_link}}</td>
                                            <td>{{$campaign->campaign->category->name}}</td>
                                            <td>
                                                <button class="btn btn-success btn-sm clipboard" data-clipboard-text="{{ $affiliate_link }}" data-message="Copied!" data-original="" data-toggle="tooltip" data-placement="bottom" title="Copy URL to clipboard">
                                                    <i class="fa fa-clipboard"></i>
                                                </button>
                                                <a class="btn btn-danger btn-sm clipboard" 
                                                    data-toggle="tooltip" 
                                                    data-placement="bottom" 
                                                    title="Remove campaign"
                                                    role="button" 
                                                    href="{{route('agent-management.destroy-user-campaign', $campaign->id)}}" 
                                                    role="button">
                                                        <i class="fa fa-trash"></i>
                                                    </a>
                                                    
                                                </button>
                                            </td>
                                            
                                        </tr>
                                    
                                    @endforeach
                                </tbody>
                            </table>     
                        </div>
                    </div>

                </div>
            </div>

        </div>

    </div>

@endsection

@section('scripts')
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
    <script src="https://cdn.datatables.net/1.10.21/js/jquery.dataTables.min.js"></script>
    <script src="https://cdn.datatables.net/1.10.21/js/dataTables.bootstrap4.min.js"></script>
    <script src="https://cdn.datatables.net/responsive/2.2.5/js/dataTables.responsive.min.js"></script>
    <script src="https://cdn.datatables.net/responsive/2.2.5/js/responsive.bootstrap4.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/clipboard.js/2.0.4/clipboard.min.js"></script>

    <script>
        $(function() {
            $('[data-toggle="tooltip"]').tooltip();
            var clipboard = new ClipboardJS('.clipboard');

            clipboard.on('success', function(e) {
                var btn = $(e.trigger);
                setTooltip(btn,'Copied!');
                hideTooltip(btn);
            });

            clipboard.on('error', function(e) {
                var btn = $(e.trigger);
                setTooltip(btn,'Failed!');
                hideTooltip();
            });
            $('#tbl-campaigns').DataTable();
        });
        function setTooltip(btn, message) {
            btn.tooltip('hide')
            .attr('data-original-title', message)
            .tooltip('show');
        }

        function hideTooltip(btn) {
            setTimeout(function() {
                btn.tooltip('hide');
            }, 1000);
        }
    </script>
@stop
