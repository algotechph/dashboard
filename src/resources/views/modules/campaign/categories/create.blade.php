@extends('layouts.admin')

@section('main-content')
    <!-- Page Heading -->
    <h1 class="h3 mb-4 text-gray-800">{{ __('Campaign Category Management') }}</h1>

    <div class="row justify-content-center">

        <div class="col-lg-12">

            <div class="card shadow mb-4">

                <div class="card-header">
                    @if (session('status'))
                        <div class="alert alert-success">
                            {{ session('status') }}
                        </div>
                    @endif
                    @if ($errors->any())
                        <div class="alert alert-danger">
                            <ul>
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div>
                    @endif
                    <h4>Create Category</h4>
                </div>

                <div class="card-body">

                    <div class="row">
                        <div class="col-lg-12">

                            <form method="POST" action="{{ route('campaign-management.store-campaign-category') }}">
                                @csrf

                                <div class="form-group">
                                    <label for="exampleInputEmail1">Category name</label>
                                    <input type="text" name="name" class="form-control" id="exampleInputEmail1" required placeholder="Category name">
                                </div>

                                <div class="form-group">
                                    <button type="submit" class="btn btn-primary">Create</button>
                                </div>
                            </form>
                            
                        </div>
                    </div>

                </div>
            </div>

        </div>

    </div>

@endsection

