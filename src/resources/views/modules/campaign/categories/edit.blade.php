@extends('layouts.admin')

@section('main-content')
    <!-- Page Heading -->
    <h1 class="h3 mb-4 text-gray-800">{{ __('Campaign Category Management') }}</h1>

    <div class="row justify-content-center">

        <div class="col-lg-12">

            <div class="card shadow mb-4">

                <div class="card-header">
                    @if (session('status'))
                        <div class="alert alert-success">
                            {{ session('status') }}
                        </div>
                    @endif
                    @if ($errors->any())
                        <div class="alert alert-danger">
                            <ul>
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div>
                    @endif
                    <h4>Update Category</h4>
                </div>

                <div class="card-body">

                    <div class="row">
                        <div class="col-lg-12">

                            <form method="POST" action="{{ route('campaign-management.update-campaign-category', $category->id) }}">
                                @csrf
                                <input class="form-control" name="id" type="hidden" value="{{ $category->id }}" >
                                <div class="form-group">
                                    <label for="exampleInputEmail1">Category name</label>
                                <input type="text" name="name" class="form-control" id="exampleInputEmail1" required placeholder="Category name" value="{{ $category->name }}">
                                </div>

                                <div class="form-group">
                                    <button type="submit" class="btn btn-primary">Update</button>
                                </div>
                            </form>
                            
                        </div>
                    </div>

                </div>
            </div>

        </div>

    </div>

@endsection

